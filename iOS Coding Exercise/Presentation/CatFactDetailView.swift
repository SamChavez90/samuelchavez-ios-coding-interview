import SwiftUI

struct CatFactDetailView: View {
    let catFact: CatFact

    var body: some View {
        VStack {
            Text(catFact.text)
                .font(.title)
            Text("Type: \(catFact.type)")
                .foregroundColor(.secondary)
        }
        .padding()
        .navigationTitle("Cat Fact Detail")
    }
}
