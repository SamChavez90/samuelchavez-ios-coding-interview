import SwiftUI

struct ContentView: View {
    // MARK: - Properties
    @State var isShowLoader : Bool = true
    @State private var catFacts: [CatFact] = [] // CatFact is your model for the response
    @State private var selectedCatFact: CatFact? = nil
    @State private var favorites: Favorites = Favorites(favoriteList: [])
    
    // MARK: - Body
    var body: some View {
        NavigationView {
            if isShowLoader {
                HStack(spacing: 15) {
                    ProgressView()
                    Text("Loading...")
                }
                .onAppear {
                    isShowLoader = true
                    fetchData()
                    _ = getFavorites()
                }
            } else {
                VStack {
                    // MARK: - Cat Facts List
                    List(catFacts) {fact in
                        NavigationLink {
                            CatFactDetailView(catFact: fact)
                        } label: {
                            HStack {
                                Text(fact.text)
                                    .lineLimit(1)
                                Spacer()
                                Button(action: {
                                    saveFavorite(fact, isAlreadyFavorite: isFavorite(fact))
                                }, label: {
                                    let imageName = isFavorite(fact) ? "star.fill" : "star"
                                    Image(systemName: imageName)
                                })
                            }
                        }
                        .buttonStyle(.plain) // Fix for clickable button in row
                    }
                    .refreshable {
                        print("Refreshing...")
                        fetchData()
                    }
                }
                .navigationTitle("Cat Facts")
            }
        }
    }

    // MARK: - Save to UserDefaults
    func saveFavorite(_ fact: CatFact, isAlreadyFavorite: Bool) {
        if isAlreadyFavorite {
            // Get index of fact to remove
            guard let index = favorites.favoriteList.firstIndex(of: fact) else { return }
            favorites.favoriteList.remove(at: index)
        } else {
            favorites.favoriteList.append(fact)
        }
        
        let defaults = UserDefaults.standard
        
        // Persist list in UserDefaults
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(favorites) {
            defaults.set(encoded, forKey: "FavoriteFacts")
            defaults.synchronize()
        }
    }
    
    func isFavorite(_ fact: CatFact) -> Bool {
        // Check if fact is saved as Favorite
        let storedFavorites = getFavorites()
        return storedFavorites.contains { $0.id == fact.id }
    }
 
    func getFavorites() -> [CatFact] {
        // Get list of favorites from UserDefaults
        if let favoriteFacts = UserDefaults.standard.object(forKey: "FavoriteFacts") as? Data {
            let decoder = JSONDecoder()
            if let factsList = try? decoder.decode(Favorites.self, from: favoriteFacts) {
                favorites.favoriteList = factsList.favoriteList
                return factsList.favoriteList
            }
        }
        return []
    }
    
    // MARK: - Fetch Data
    func fetchData() {
        print("fetching")
        guard let url = URL(string: "https://cat-fact.herokuapp.com/facts") else { return }
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let data = data {
                do {
                    let catFacts = try JSONDecoder().decode([CatFact].self, from: data)
                    DispatchQueue.main.async {
                        self.catFacts = catFacts
                        self.isShowLoader = false
                    }
                } catch {
                    print("Error decoding JSON: \(error)")
                }
            } else if let error = error {
                print("Error fetching data: \(error)")
            }
        }
        .resume()
    }
}

// MARK: - Preview
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct Favorites: Codable {
    var favoriteList: [CatFact]
}
